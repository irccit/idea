if [[ ! -o interactive ]]; then
    return
fi

compctl -K _idea idea

_idea() {
  local word words completions
  read -cA words
  word="${words[2]}"

  if [ "${#words}" -eq 2 ]; then
    completions="$(idea commands)"
  else
    completions="$(idea completions "${word}")"
  fi

  reply=("${(ps:\n:)completions}")
}
