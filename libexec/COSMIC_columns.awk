# vim: set softtabstop=4 shiftwidth=4 filetype=awk expandtab:

BEGIN {
    FS = "\t";
    OFS="\t"
}

!/^#/ {
    type = $1 # exonic, intergenic, extragenic, ...
    effect = $2
    if (type == "exonic") {
        split($3, a, ",");
        codon_change = $4
        codon_usage_ratio = $5
        split(a[1], info, ":") # prendo il primo
        gene = info[1]
        accession = info[2];
        exon = info[3];
        Nchange = info[4];
        AAchange = info[5];
        patt = $10 "[0-9]+" $11;
        fwd_rev = (Nchange ~ patt) ? 1 : -1;
    } else {
        gene = "??"
        if (type != "extragenic") {
            split($3, a, ",")
            split(a[1], info, ":")
            gene = info[1]
        }
        AAchange = "."
        Nchange = "."
        accession = "."
        codon_change = "."
        codon_usage_ratio = "."
        fwd_rev = "."
    }
    chr_pos = $19

    print chr_pos, gene, type, effect, AAchange, Nchange, accession, codon_change, codon_usage_ratio, gene, fwd_rev, $0
}
