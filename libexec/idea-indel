#!/usr/bin/env bash
# vim: set softtabstop=2 shiftwidth=2 filetype=sh expandtab:
# Usage: idea indel
# Summary: Run indel call
# Help: This command is used to run the indel call between two samples to get
# a report of insertions and deletions gained by the second sample.

set -e
set -u
set -o pipefail

source "$_IDEA_ROOT/lib/logging"

export LC_ALL=C

progname="$(basename $0)"

eval "$("$_IDEA_ROOT/libexec/parseargs" "$progname" \
  ""  "aln1"      1 1 "NA"   "Sample 1 alignment file." \
  ""  "aln2"      1 1 "NA"   "Sample 2 alignment file." \
  "b" "bed"       1 1 "NA"   "BED file of regions where indel should be called." \
  "C" "idea-conf" 1 1 "NA"   "IDEA configuration." \
  "o" "outdir"    0 1 "$PWD" "Output directory" \
  ""  "sample1"   1 1 "NA"   "Sample 1 name." \
  ""  "sample2"   1 1 "NA"   "Sample 2 name." \
  "t" "threads"   0 1 1      "Number of threads." \
  "w" "work-dir"  0 1 "NA"   "Working directory." \
  "--" \
  "$@")"

if [ ! -e "$aln1" ]; then
  fail "$progname: $aln1 does not exist"
  exit 1
fi

if [ ! -e "$aln2" ]; then
  fail "$progname: $aln2 does not exist"
  exit 1
fi

if [ ! -e "$bed" ]; then
  fail "$progname: $bed does not exist"
  exit 1
fi

sample="${sample1}__${sample2}"

mkdir -p "$outdir"

orig_outdir="$outdir"

if [ "$work_dir" != "NA" ]; then
  outdir="$(mktemp -d "$work_dir/$(basename $0).XXXXXX")"
fi

bed_region_temp="$(mktemp -p "$outdir" --suffix=".coding.bed" "$(basename "$bed" ".bed").XXXXXXXXXX")"

bed_pos_temp="$(mktemp -p "$outdir" --suffix=".pos.bed" "$(basename "$bed" ".bed").XXXXXXXXXX")"

bed_pindel_prefix="$(mktemp -u -p "$outdir" bed_pindel_region.XXXXXXXXXX.)"

function cleanup() {
  rm -f "$bed_region_temp" \
    "$bed_pos_temp" \
    "$bed_pindel_prefix"*

  if [ "$work_dir" != "NA" ]; then
    mv "$outdir"/* "$orig_outdir"
  fi

  if [ "$work_dir" != "NA" ]; then
    rm -rf "$outdir"
  fi
}

trap cleanup ERR EXIT

pindel_config="$outdir/pindel_config_${sample}.txt"

echo -e "$aln1\t500\t$sample1" > "$pindel_config"

sort -k1,1 -k2,2n "$bed" | \
mergeBed | \
grep -v _ > "$bed_region_temp"

awk 'BEGIN {
  OFS = "\t"
}
{
  x = int($2 + ($3 - $2) / 2)

  print $1, x, x + 1
}' "$bed_region_temp" > "$bed_pos_temp"

cut -f 1 "$bed" | \
sort -u | \
grep -v _ | \
parallel -j "$threads" \
  "samtools view -u $aln1 {} | samtools depth -a -b $bed_pos_temp -" | \
sort -k1,1 -k2,2n > "$outdir/${sample}.depth"


paste "$bed_region_temp" "$outdir/${sample}.depth" | \
cut -f 1-3,6 | \
awk 'BEGIN {
  OFS = "\t"
}
{
  print $1, $2, $3, "xx", $4
}' | \
"$_IDEA_ROOT/libexec/cluster_BED_regions.py" - 600 | \
split -d -l 1 -a 4 - "$bed_pindel_prefix"

ls -1 "$outdir/$bed_pindel_prefix."*.bed | \
parallel -j "$threads" \
  "pindel -f $genome_seq -j {} -T 1 -i $pindel_config -o $outdir/${sample}.{/} > $outdir/err_pindel_${sample}.{/}"

mkdir "$outdir/log_${sample}"

mv -i "$outdir/err_pindel."* "$outdir/log_$sample"

mkdir "$outdir/check_${sample}"

mv "$outdir/"*chr[0-9XY]* "$outdir/check_$sample"

echo -e "REGION\t" \
  "GENE-NAME\t" \
  "INDEL\t" \
  "TYPE\t" \
  "VARIATION\t" \
  "FREQUENCIES\t" \
  "READS\t" \
  "NOT_ALTERED\t" \
  "ALTERED\t" \
  "SEQUENCE\t" \
  "REPEAT_UPSTREAM\t" \
  "REPEAT_DOWNSTREAM\t" > "$outdir/${sample}.check.Indel"

grep ^[0-9] "$outdir/check_${sample}/"*_D | \
awk -v count_wt_normal=0 \
  -v freq=0 \
'BEGIN {
  OFS = "\t"
  print
}
{
  if ($33 >= $34) {
    count_wt_normal = $33
  } else {
    count_wt_normal = $34
  }

  if ($3 % 3 == 0) {
    variation = "in-frame"
  } else {
    variation = "frameshift"
  }

  if (count_wt_normal + $35 + $35 > 0) {
    freq = (($35 + $37) * 100) / (count_wt_normal + $35 + $37)
  } else {
    freq = 0
  }

  print $8":"$10"-"$11,
    "GENE-NAME",
    "Deletion",
    $2"-"$3,
    variation,
    freq,
    count_wt_normal + $35 + $37,
    count_wt_normal,
    $35 + $37,
    $6,
    $13 - $10,
    $14 - $11
}' | \
sort -u >> "$outdir/${sample}.check.Indel"

grep ^[0-9] "$outdir/check_${sample}/"*_SI | \
awk -v count_wt_normal=0 \
  -v freq = 0 \
'BEGIN {
  OFS = "\t"
  print
}
{
  if ($33 >= $34) {
    count_wt_normal = $33
  }
  else {
    count_wt_normal =$ 34
  }

  if ($3 % 3 == 0) {
    variation = "in-frame"
  } else {
    variation = "frameshift"
  }

  if (count_wt_normal + $35 + $37 > 0) {
    freq = (($35 + $37) * 100) / (count_wt_normal + $35 + $37)
  } else {
    freq = "0"
  }

  print $8":"$10"-"$11,
    "GENE-NAME",
    "Insertion",
    $2"-"$3,
    variation,
    freq,
    count_wt_normal + $35 + $37,
    count_wt_normal,
    $35 + $37,
    $6,
    $13 - $10,
    $14 - $11
}' | sort -u >> "$outdir/${sample}.check.Indel"

join -a 1 -o auto \
  <(grep -v ^$ "$outdir/${sample}.check.Indel" | \
    sort -bu | \
    awk 'NR > 1 {
OFS = "\t";
split($1, a, "-")
print a[1], $1, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12
}' | \
    sort -u) \
  <(awk '{
OFS = "\t"
for(i = $2 + 1; i <= $3; i++) { print $1":"i, $4 }
}' "$bed" | \
    tr "_" " " | \
    awk '{
OFS = "\t"
print $1, $2
}' | \
    sort -u) |
awk 'BEGIN {
OFS = "\t"
print "REGION",
  "GENE-NAME",
  "INDEL",
  "TYPE",
  "VARIATION",
  "FREQUENCIES",
  "READS",
  "NOT_ALTERED",
  "ALTERED",
  "SEQUENCIES",
  "TR_UPSTREAM",
  "TR_DOWNSTREAM"
}
{
  print $2, $13, $3, $4, $5,$6, $7, $8, $9, $10, $11, $12
}' > "$outdir/${sample}.Indel"

"$_IDEA_ROOT/libexec/annotate_indel_cosmic_noref.sh" "$cosmic_indel" "$outdir/${sample}.Indel"

"$_IDEA_ROOT/libexec/annotate_indel_cosmic_noref_0.sh" "$cosmic_indel" "$outdir/${sample}.check.Indel"

exit 0
