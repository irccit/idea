#!/usr/bin/env bash
# vim: set softtabstop=2 shiftwidth=2 filetype=sh expandtab:

set -e
#set -u #XXX togliere altrimenti Rscript non funziona
set -o pipefail

if [ $# -lt 1 ]; then
	echo "Usage: $0 CNfile|- other_staff...." >&2
	exit 1
fi

##### in caso di Ctrl-C....
control_c()
{
	if [ -e "$DATA_FILE" ]; then
		echo Removing temp files... >&2
		rm -f $DATA_FILE
	fi
	exit
}
trap control_c SIGINT SIGTERM

PROGRAM_NAME=$(basename $0)
CNfile=$1
shift
other="$@"

export DATA_FILE=`mktemp --tmpdir=$PWD __tmp__$PROGRAM_NAME\__.XXXXXXXXXXXX`
sort -T . -S1G -k1,1 -k2,2n -u $CNfile | awk '$7>0' > $DATA_FILE

if [ -s "$DATA_FILE" ]; then

	if [ "`cat $DATA_FILE | wc -l`" -gt 1 ]; then
#	echo OK $other >&2
	(
	Rscript -<< EOF $DATA_FILE
library(DNAcopy)
args <- commandArgs(TRUE)
f = args[1]
cn = read.table(f, sep="\t" )

CNA.object <-CNA( genomdat = log(cn[,7])/log(2), chrom = cn[,1], maploc = cn[,2], data.type="logratio")
#CNA.object <-CNA( genomdat = cn[,7], chrom = cn[,1], maploc = cn[,2])
CNA.smoothed <- smooth.CNA(CNA.object)
segs <- segment(CNA.smoothed, undo.splits="sdundo", undo.SD=1, verbose=0, min.width=5)
segs2 = segs$output
print(segs2)
EOF
	) | \
		awk 'BEGIN{OFS="\t"} $1 + 0 == $1 {print $3,$4,$5,$7,$6}'
	else # scrive l'unica riga che c'e'
		awk 'BEGIN{FS="\t"; OFS="\t"} $2 + 0 == $2 {print $1,$2,$3,$7,1}' $DATA_FILE
	fi
fi

rm -f $DATA_FILE
