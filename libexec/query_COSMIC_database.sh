# vim: set softtabstop=2 shiftwidth=2 filetype=sh expandtab:

set -e
set -u
set -o pipefail

# per interrogare il database di COSMIC

if [ $# -ne 2 ]; then
  echo "Usage: $0 db_file.sql3 input|-" >&2
  echo "Input: chrom pos geneName" >&2
  exit 1
fi

INPUT=$2
COSMICDB=$1

(
awk 'BEGIN {
  print ".mode tabs"
  print "create temp table INPUT (chrom str, pos int, geneName text);"
}
{
  print "insert into INPUT values (\"" $1 "\"," $2 ",\"" $3 "\");"
}' $INPUT

cat << EOF
select X1.chrom,
  X1.pos,
  X1.geneName,
  CASE WHEN (X1.occurrence + X2.geneInCOSMIC < 0) THEN '--' ELSE X1.occurrence + X2.geneInCOSMIC END AS 'occurrence',
  X2.yn,
  X2.totPos,
  X2.totPosUniq
from (
select INPUT.*,
  CASE WHEN XX.occurrence is null THEN 0 ELSE XX.occurrence END AS 'occurrence',
  CASE WHEN XX.yn is null THEN '??' ELSE XX.yn END AS 'yn',
  CASE WHEN XX.totPos is null THEN '??' ELSE XX.totPos END AS 'totPos',
  CASE WHEN XX.totPosUniq is null THEN '??' ELSE XX.totPosUniq END AS 'totPosUniq'
  from INPUT left join (
    select A.*,B.yn,B.totPos,B.totPosUniq 
    from pos_count_gene as A,gene_intestine_totPos_totPosUniq as B
    where A.geneName = B.geneName) as XX on INPUT.chrom = XX.chrom and INPUT.pos = XX.pos
) as X1, (
select INPUT.*,
  CASE WHEN XX.geneName is null THEN -1 ELSE 0 END AS 'geneInCOSMIC',
  CASE WHEN XX.yn is null THEN '??' ELSE XX.yn END AS 'yn',
  CASE WHEN XX.totPos is null THEN '??' ELSE XX.totPos END AS 'totPos',
  CASE WHEN XX.totPosUniq is null THEN '??' ELSE XX.totPosUniq END AS 'totPosUniq'
  from INPUT left join gene_intestine_totPos_totPosUniq as XX on INPUT.geneName = XX.geneName
) as X2
where X1.chrom = X2.chrom and X1.pos = X2.pos;
EOF
) | sqlite3 $COSMICDB
