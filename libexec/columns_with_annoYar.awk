# vim: set softtabstop=4 shiftwidth=4 filetype=awk expandtab:

BEGIN {
    FS = "\t"
    OFS = "\t"

    # revcomp bases
    b["A"] = "T"
    b["C"] = "G"
    b["G"] = "C"
    b["T"] = "A"
    b["N"] = "N"

    # posizione conte del campione 1
    pos["A"] = 18
    pos["C"] = 19
    pos["G"] = 20
    pos["T"] = 21

    # header
    print "occur_pos_in_COSMIC", \
          "gene_name", \
          "DESCRIPTION", \
          "AAchange", \
          "var_effect", \
          "%_mutant_reads", \
          "Nchange", \
          "accession", \
          "gene_ref_base", \
          "ref_base", \
          "call", \
          "gain_loss", \
          "coord", \
          "large_intestine?", \
          "num_occur_gene", \
          "num_occur_uniq", \
          "A1", \
          "A2", \
          "C1", \
          "C2", \
          "G1", \
          "G2", \
          "T1", \
          "T2", \
          "depth1", \
          "depth2", \
          "delta_freq", \
          "allele", \
          "ratio_freq", \
          "allele", \
          "p-value", \
          "allele", \
          ##--## aggiunte :)
          "type", \
          "codon_change", \
          "codon_usage_ratio"
}

{
    # 1 type
    # 2 effect
    # 3 AAchange
    # 4 NTchange
    # 5 accession
    # 6 codon_change
    # 7 codon_usage_ratio
    # 8 gene (from ANNOyAR)
    # 9 strand
    # 10... output di Enzo-filter

    gain_loss = $27
    changed_allele = substr(gain_loss, length(gain_loss), 1)
    gene_ref_base = ($9 == 1) ? $17 : b[$17]
    CN1 = $18 + $19 + $20 + $21
    CN2 = $22 + $23 + $24 + $25
    freq_changed_allele = 100 * ($(pos[changed_allele] + 4) / CN2)

    printf "%s\t%s\t%s", $29, $8, $34 # COSMIC_occorr_pos, gene, descr (era $30)
    printf "\t%s\t%s", $3, $2 # AAchange, ANNOVAR
    printf "\t%s", freq_changed_allele # % mutant reads
    printf "\t%s\t%s", $4, $5 # Nchange, accession
    printf "\t%s\t%s", gene_ref_base, $17 # gene_ref_base, chrom_ref_base

    printf "\t%s\t%s", $26, gain_loss # call, gain/loss
    printf "\t%s", $16 # coord
    printf "\t%s\t%s\t%s", $30, $32, $33 # COSMIC: large_intestine, occorr pos gene, occorr pos uniq

    printf "\t%g\t%g", $18, $22 # conte riordinate..
    printf "\t%g\t%g", $19, $23
    printf "\t%g\t%g", $20, $24
    printf "\t%g\t%g", $21, $25

    printf "\t%s\t%s", CN1, CN2 # CN

    for (i = 10; i <= 15; i++)
        printf "\t%s", $i # parametri

    printf "\t%s\t%s\t%s", $1, $6, $7 # type (exonic, intergenic....), codon_change, codon_usage_ratio
    print ""
}
