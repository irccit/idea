#!/usr/bin/env bash
# vim: set softtabstop=2 shiftwidth=2 filetype=sh expandtab:

set -e
set -u
set -o pipefail

export LC_ALL=C

if [ $# -ne 2 ]; then
  echo "Usage: $(basename $0) <cosmic_indel> <file.check.Indel>" 1>&2
  exit 1
fi

file_indel="$2"
cosmic_indel="$1"

outdir="$(dirname "$file_indel")"

(
  join -a 1 \
    -e '--' \
    -o auto \
    <(awk '$3 ~ "Deletion"' "$file_indel" | sort) \
    <(awk '$3 ~ "Deletion"' "$cosmic_indel" | sort)

  join -a 1 \
    -e "--" \
    -o auto \
    <(awk '$3 ~ "Insertion"' "$file_indel" | sort) \
    <(awk '$3 ~ "Insertion"' "$cosmic_indel" | sort) \

  join -e "--" \
    -o auto \
    <(sort "$file_indel") \
    <(awk '$3i ~ "Complex_Frameshift"' "$cosmic_indel" | sort)
) | \
awk 'BEGIN {
  FS = " "
  OFS = "\t"
}
{
  print $18,
    $1,
    $2,
    $3,
    $4,
    $5,
    $6,
    $7,
    $8,
    $9,
    $10,
    $11,
    $12,
    $13,
    $14,
    $15,
    $16,
    $17
}' | \
sort -k1,1rn | \
uniq | \
awk 'BEGIN {
  OFS = "\t"
  print "OCCUR_COSMIC",
    "COORD",
    "GENE",
    "TYPE",
    "INDEL",
    "EFFECT",
    "% ALTERED READS",
    "TOT_READS1",
    "WT1",
    "ALTERED1",
    "SEQUENCE",
    "TR_UP",
    "TR_DOWN",
    "COSMIC_GENE_NAME",
    "MUTATION_DESCRIPTION",
    "MUTATION_CDS",
    "MUTATION_AA",
    "COSMIC_ID"
}
{
  print
}' > "$outdir/$(basename "$file_indel" .Indel).cosmic.Indel"

exit 0
