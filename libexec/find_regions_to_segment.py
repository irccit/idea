#!/usr/bin/python
# -*- coding: utf-8 -*-
# vim: set softtabstop=4 shiftwidth=4 expandtab:

import argparse
import fileinput
import sys
import Segmenti

MAXDIST = 2000000 # 10000000

def main():
    desc = "Generate segments for gene copy number plot."

    parser = argparse.ArgumentParser(description = desc)

    parser.add_argument("refflat", help = "UCSC refFlat full table file.")

    args = parser.parse_args()

    refflat = [row.strip().split() for row in fileinput.input(args.refflat)]
    refflat.pop(0) # Remove header

    data = {}
    for i in refflat:
        chrom = i[2]
        start = int(i[4]) + 1 # refFlat is 0-based
        end = int(i[5])
        if not data.has_key(chrom):
            data[chrom] = []
        data[chrom].append((start, end))

    for chrom,coords in data.items():
        s = Segmenti.Segmenti(coords, MAXDIST)
        seg = s.segmenti
        for i in seg:
            print "%s\t%d\t%d" % (chrom, i[0] - 100, i[1] + 100)

        #  seg.sort()

        #  e = seg[0][1]
        #  for i in seg[1:]:
        #      d = i[0] - e
        #      if d >= MINDIST:
        #          print "%s\t%d\t%d" % (chrom, e + 1, i[0] - 1)
        #      e = i[1]

if __name__ == '__main__':
    main()
