#!/usr/bin/python
## ANNOYAR - a clone for ANNOVAR
import sys
import os
import gzip
import urllib2
from utils import *
from Bio import Seq
import subprocess
import operator

if len(sys.argv) != 1+3:
	sys.exit("""
	Usage:
		%s --from-file local_file.yar input_file|-
		%s --retrieve-file genome_symbol output_directory

	(genome_symbol could be: hg18, hg19, mm10, ....)

	"--from-file" will send output to STDOUT
	"input_file" is TAB-separated:
	-chromosome
	-position1
	-position2
	-FROM_allele
	-TO_allele
	-[free fields...]

	"--retrieve-file" will save two files:
		-refFlat_mRNA.<GENOME_SYMBOL>.yar
		-<GENOME_SYMBOL>.fa

	In the .yar file, uppercase bases are coding,
	while lowercase bases are non-coding (UTR).

	""" % (sys.argv[0],sys.argv[0]))

DEBUG = False # metti a True per avere la lista delle isoforme scartate! XXX

OPTIONS = ["--from-file","--retrieve-file"]
BASE_REFFLAT = "http://hgdownload.cse.ucsc.edu/goldenPath/%s/database/refFlat.txt.gz"
#BASE_GENOME = "http://hgdownload.cse.ucsc.edu/goldenPath/%s/bigZips/*chromFa.tar.gz"
BASE_GENOME = "http://hgdownload.cse.ucsc.edu/goldenPath/%s/bigZips/{,%s.}chromFa.tar.gz"

################# funzioni #####################
def retrieve_correct_mRNA(refFlat_stream,DNA_file,out_file):
#	fd_refFlat = open(refFlat_file)
	fd_out = open(out_file,'w')

	refFlat = {}
	N = 0
	for row in [i.strip().split('\t') for i in refFlat_stream.readlines()]:
		N += 1
		if N==1 and "txStart" in row: # header
			continue
		gene,acc,chrom,strand,txS,txE,cdsS,cdsE,exN,exS,exE = row[:11]
		txS = int(txS)
		txE = int(txE)
		cdsS = int(cdsS)
		cdsE = int(cdsE)
		exN = int(exN)
		exS = [int(i) for n,i in enumerate(exS.split(',')) if n<exN] # 0-based
		exE = [int(i) for n,i in enumerate(exE.split(',')) if n<exN]
		if not refFlat.has_key(chrom):
			refFlat[chrom] = []
		refFlat[chrom].append({"acc": acc,
				"txS": txS,
				"txE": txE,
				"cdsS": cdsS,
				"cdsE": cdsE,
				"exS": exS,
				"exE": exE,
				"strand": strand,
				"row": row # per comodita'
				})

	chroms_in_refFlat = refFlat.keys()
	# OK! Ora leggo i vari cromosomi, uno alla volta, e ricreo gli mRNA
	for DNA in leggiFASTA(DNA_file):
###	NM_001136003
		chrom_name = DNA.name
		print >>sys.stderr, "Working on:", chrom_name
		if not refFlat.has_key(chrom_name):
			print >>sys.stderr, "WARNING: no '%s' in refFlat file. Skipping." % chrom_name
			continue
		chroms_in_refFlat.remove(chrom_name)
		for v in refFlat[chrom_name]:
			txS = v["txS"]
			txE = v["txE"]
			cdsS = v["cdsS"]
			cdsE = v["cdsE"]
			seq = Seq.Seq("")
			for pos in zip(v["exS"],v["exE"]):
				_exS, _exE = pos
				seq_to_add = DNA.seq[_exS:_exE].upper()
				if _exS+1<cdsS+1:
					if cdsS+1<_exE: # cdsS compreso nell' esone
						to_lower = cdsS-_exS
						seq_to_add = seq_to_add[:to_lower].lower()+seq_to_add[to_lower:]
					else: # esone intero
						seq_to_add = seq_to_add.lower()
				if _exE>cdsE: # non deve essere un 'elif' per considerare anche il caso di due pezzi di UTR sullo stesso esone
					if cdsE>_exS+1: # cdsE compreso nell' esone
						to_upper = cdsE-(_exS+1)+1
						seq_to_add = seq_to_add[:to_upper]+seq_to_add[to_upper:].lower()
					else: # esone intero
						seq_to_add = seq_to_add.lower()
				seq += seq_to_add
			if v["strand"] == '-':
				seq = seq.reverse_complement()
			extended_accession = ">%s/%s:%d-%d" % (v["acc"],chrom_name,txS+1,txE)
			print >>fd_out, extended_accession, '\t'.join(v["row"])
			print >>fd_out, pretty_seq(str(seq))

	# scrivo i cromosomi presenti in refFLat ma non nel file FASTA
	for i in chroms_in_refFlat:
		print >>sys.stderr, "WARNING: no '%s' in sequence file: skipped." % i

def search_for_transcript(chrom,pos,all_transcripts):
# cerca i trascritti che intersecano quella coordinata
	matches = []
	for n,i in enumerate(all_transcripts[chrom]):
		if i[0]<=pos<=i[1]:
			matches.append(i)
			# controllo se c'e' ANCHE nel prossimo esone
			for j in all_transcripts[chrom][n+1:]:
				if i[1]<j[0]:
					break
				if (j[0]<=pos<=j[1]):
					matches.append(j)
			break
	# restituisce l'elenco dei trascritti
	return matches and [i[2] for i in matches] or matches

def get_info_on_position(accession,pos,refGene):
# dato un trascritto e una posizione, annota la posizione anche tramite le posizioni relative sul trascritto e sulla CDS (1-based)
	transcript = refGene[accession]
	strand = transcript["strand"]
	N = 0 # tx_pos
	t = "ERROR"
	exon_number = 0
	if strand == '+': # fwd
		for n,i in enumerate(zip(transcript["exS"],transcript["exE"])):
			s,e = i
			if s<=pos<=e:
				N += pos-s+1
				coding_pos = N-transcript["CDS_shift"]
				exon_number = n+1
				if transcript["cdsS"]-transcript["cdsE"]==1: # era transcript["cdsS"]>transcript["cdsE"]:
					t = "ncRNA"
				elif transcript["cdsS"]<=pos<=transcript["cdsE"]:
					t = "exonic"
				elif pos<transcript["cdsS"]:
					t = "5_UTR"
				elif pos>transcript["cdsE"]:
					t = "3_UTR"
				else:
					sys.exit("Problema!")
				break
			N += e-s+1
		else:
			t = "intronic"
			#N = 0 # resetta
			coding_pos = 0 # resetta
	else: # rev
		for n,i in enumerate(reversed(zip(transcript["exS"],transcript["exE"]))):
			s,e = i
			if s<=pos<=e:
				N += e-pos+1 #pos-s+1
				coding_pos = N-transcript["CDS_shift"]
				exon_number = n+1
				if transcript["cdsS"]-transcript["cdsE"]==1: # era transcript["cdsS"]>transcript["cdsE"]:
					t = "ncRNA"
				elif transcript["cdsS"]<=pos<=transcript["cdsE"]:
					t = "exonic"
				elif pos<transcript["cdsS"]:
					t = "3_UTR"
				elif pos>transcript["cdsE"]:
					t = "5_UTR"
				else:
					sys.exit("Problema!")
				break
			N += e-s+1
		else:
			t = "intronic"
			#N = 0 # resetta
			coding_pos = 0 # resetta
	info = refGene[accession].copy()
	info["pos_type"] = t
	info["tx_pos"] = N # basi dall'inizio dell'mRNA
	info["coding_pos"] = coding_pos # numero di basi codificanti
	info["exon_number"] = exon_number
	# return
	return info
		#{
		#"pos_type":t,
		#"tx_pos": N, # basi dall'inizio dell'mRNA
		#"coding_pos": coding_pos, # numero di basi codificanti
		#"exon_number": exon_number
		#}

def get_codon_position(x):
# restituisce la posizione all'interno del codone;
	return (x-1)%3 # 0-based

def get_codons(tx_pos,coding_pos,mRNA_seq,from_allele,to_allele):
# occhio! Qui e' tutto relativo al mRNA!
# trova le basi che variano
# tx_pos: 1-based
# coding_pos: 1-based
	p = get_codon_position(coding_pos) # 0-based
	s,e = tx_pos-1-p,tx_pos-1-p+3 # 0-based
	# codone del riferimento
	gene_ref_base = mRNA_seq[tx_pos-1].upper()
	ref_codon = mRNA_seq[s:e].upper()
	# codone del FROM
	from_codon = list(ref_codon)
	from_codon[p] = from_allele.upper()
	from_codon = ''.join(from_codon)
	# codone del TO
	to_codon = list(ref_codon)
	to_codon[p] = to_allele.upper()
	to_codon = ''.join(to_codon)
	return {
		'gene_ref_base': gene_ref_base,
		'ref_codon': ref_codon,
		'from_codon': from_codon,
		'to_codon': to_codon
		}

def get_CDS_shift(refGene_record):
# calcola quante sono le basi del 5' UTR (le prime non coding)
	x = refGene_record
	N = 0
	if x["strand"] == '+': # fwd
		cdsS = x["cdsS"]
		for exon in zip(x["exS"],x["exE"]):
			s,e = exon
			if e<cdsS: # UTR completo
				N += e-s+1
			elif cdsS<=e: # UTR incompleto
				N += cdsS-s # non aggiungere '-1'
				break
			else:
				break
	else: # rev
		cdsE = x["cdsE"]
		for exon in reversed(zip(x["exS"],x["exE"])):
			s,e = exon
			if cdsE<s: # UTR completo
				N += e-s+1
			elif s<=cdsE: # UTR incompleto
				N += e-cdsE # non aggiungere '-1'
				break
			else:
				break
	return N

def get_effect(AA_from,AA_to):
# che effetto ha la variazione?
	if AA_from == AA_to:
		return "synonymous"
	elif AA_from == '*':
		return "stoploss"
	elif AA_to == '*':
		return "stopgain"
	else:
		return "nonsynonymous"

def annoy(fd_input,fd_yar):
	print >> sys.stderr, "Parsing the .yar file ...."
	refGene = {}
	N_refGene = 0
	extended_accession = None
	for i in fd_yar:
		i = i.strip()
		if i[0]!='>': # aggiungo alla sequenza
			refGene[extended_accession]["mRNA"].append(i.upper())
			continue
		if extended_accession is not None:
			# unisco la sequenza
			x = ''.join(refGene[extended_accession]["mRNA"])
			refGene[extended_accession]["mRNA"] = x

		# divido la riga opportunamente
		x = i[1:].split(' ') # prima con uno spazio...
		extended_accession = x[0]
		refGene_fields = (' '.join(x[1:])).split('\t') # ...poi con TAB
		N_refGene += 1
		gene,acc,chrom,strand,txS,txE,cdsS,cdsE,exN,exS,exE = refGene_fields[:11]
		exN = int(exN)
		txS = int(txS)
		txE = int(txE)
		cdsS = int(cdsS)
		cdsE = int(cdsE)
		exS_list = [int(i)+1 for n,i in enumerate(exS.split(',')) if n<exN] # 1-based
		exE_list = [int(i) for n,i in enumerate(exE.split(',')) if n<exN]
		tx_info = {
			"gene": gene,
			"chrom": chrom,
			"strand": strand,
			"txS": txS+1, # 1-based
			"txE": txE,
			"cdsS": cdsS+1, # 1-based
			"cdsE": cdsE,
			"exN": exN,
			"exS": exS_list,
			"exE": exE_list,
			"mRNA": []
			}
		tx_info["CDS_shift"] = get_CDS_shift(tx_info)
		refGene[extended_accession] = tx_info

	# unisco l'ultima sequenza
	x = ''.join(refGene[extended_accession]["mRNA"])
	refGene[extended_accession]["mRNA"] = x

	# controllo tutte le isoforme
	wrongAnnotation = 0 # conta le isoforme scartate
	for acc,tx in refGene.items():
		coding_length = 0
		tx_length = 0
		for i in zip(tx["exS"],tx["exE"]):
			if i[1]>=tx["cdsS"] and i[0]<=tx["cdsE"]:
				s = max([tx["cdsS"],i[0]])
				e = min([tx["cdsE"],i[1]])
				coding_length += e-s+1
			tx_length += i[1]-i[0]+1
		refGene[acc]["CDS_length"] = coding_length
		refGene[acc]["tx_length"] = tx_length
		if (coding_length%3)!=0:
			N_refGene -= 1
			wrongAnnotation += 1
			if DEBUG: print >>sys.stderr, "WrongISO:", acc, tx["gene"], "coding_length:", coding_length
			# cancello
			del refGene[acc]

	print >>sys.stderr, "INFO: refGene total records:", N_refGene
	if wrongAnnotation > 0:
		print >>sys.stderr, "WARNING: removed wrong annotated isoforms:", wrongAnnotation

	# preparo la tabella degli esoni centrata sui cromosomi (per la ricerca veloce)
	all_transcripts = {}
	for acc,tx in refGene.items():
		chrom = tx["chrom"]
		if not all_transcripts.has_key(chrom):
			all_transcripts[chrom] = []
		data = [
			tx["txS"], # start
			tx["txE"], # end
			acc # accession
			]
		all_transcripts[chrom].append(data)
	# ora ordino per posizione
	for k,v in all_transcripts.items():
		v.sort() # in-place sort

	available_chroms = all_transcripts.keys()

# parse input and do the annoying part ;)
	print >> sys.stderr, "Reading the input file ...."
	effect_order = [
			"stopgain",
			"stoploss",
			"nonsynonymous",
			"synonymous"
			]
	type_order = [
			"5_UTR",
			"3_UTR",
			"ncRNA",
			"intronic"
			]
	wrong_input_rows = []
	for row in fd_input:
		row = row.strip().split('\t')
		chrom,pos1,pos2,FROM,TO = row[:5]
		if chrom not in available_chroms:
			print >>sys.stderr, "WARNING: no '%s' chromosome in .yar file. Skipping input row: %s" % (chrom,'\t'.join(row[:5]))
			wrong_input_rows.append((chrom,pos1,pos2))
			continue
		if pos1!=pos2 or len(FROM)+len(TO)!=1+1:
			sys.exit("ERROR: You must specify (for now) a 1-base position! [Wrong input: %s]" % '\t'.join([chrom,pos1,pos2,FROM,TO]))
		pos = int(pos1)
		other = row[5:]
		# annoy!
		accession_found = search_for_transcript(chrom,pos,all_transcripts)
		#real_ref_base = '.'
		if not accession_found:
			effect = "NA"
			variations = '.'
			#change = "%s:%s>%s" % ('?',FROM,TO)
			print "%s\t%s\t%s\t%s\t%s\t%s\t%s" % (
							"extragenic",
							effect,
							variations,
							#change,
							'.', # il cambio di codone
							'.', # codon_usage_ratio
							'?', # real_ref_base
							'\t'.join(row)
							)
			continue
		accessions_info = []
		for acc in accession_found:
			info = get_info_on_position(acc,pos,refGene)
			gene = refGene[acc]["gene"]
			strand = refGene[acc]["strand"]
			info["acc"] = acc # tengo traccia dell'accession
			info["gene"] = gene
			info["strand"] = strand
		#	pos_type = info["pos_type"]
			tx_pos = info["tx_pos"]
			coding_pos = info["coding_pos"]
			cFROM = (strand=='-') and revcomp(FROM) or FROM
			cTO = (strand=='-') and revcomp(TO) or TO
			info["cFROM"] = cFROM
			info["cTO"] = cTO
			if info["pos_type"] == "exonic":
				#codons = get_codons(tx_pos,coding_pos,mRNA[acc],cFROM,cTO)
				codons = get_codons(tx_pos,coding_pos,refGene[acc]["mRNA"],cFROM,cTO)
				AA_from = translate(codons["from_codon"],info=info)
				AA_to = translate(codons["to_codon"],info=info)
				AA_ref = translate(codons["ref_codon"],info=info)
				effect = get_effect(AA_from,AA_to)
				info["AA_from"] = AA_from
				info["AA_to"] = AA_to
				info["AA_ref"] = AA_ref
				info["effect"] = effect
				info["AA_pos"] = 1+int((coding_pos-1)/3)
				info["gene_ref_base"] = codons["gene_ref_base"]
				info["from_codon"] = codons["from_codon"]
				info["to_codon"] = codons["to_codon"]
				accessions_info.append(info)
			else:
				# metto valori inutili come default
				info["AA_from"] = '.'
				info["AA_to"] = '.'
				info["AA_ref"] = '.'
				info["effect"] = "NA"
				accessions_info.append(info)
		# scegliere le info da riportare (precedenza a EXON)
		exons = [i for i in accessions_info if i["pos_type"]=="exonic"]
		#change = ['?',FROM,TO]
		if exons:
			# raggruppo gli esoni
			effects = [i["effect"] for i in exons]
			for i in effect_order:
				if effects.count(i):
					best_effect = [j for j in exons if j["effect"]==i]
					break
			gene_ref_base = best_effect[0]["gene_ref_base"]
			strand = best_effect[0]["strand"]
			#change[0] = (best_effect[0]["strand"]=='-') and revcomp(gene_ref_base) or gene_ref_base
			bb = (best_effect[0]["strand"]=='-') and revcomp(gene_ref_base) or gene_ref_base
#			if FROM != bb:
#				real_ref_base = bb
			real_ref_base = (FROM!=bb) and bb or '.'
			# ordino secondo il cds/tx piu' lungo
			best_effect = [((i["CDS_length"],i["tx_length"]),i) for i in best_effect] # aggingo la tupla: CDS_length,tx_length
			best_effect.sort(reverse=True) # ordino dal piu' lungo al piu' corto
			best_effect = [i[1] for i in best_effect] # ripristino togliendo l'aggiunta
			var_info = ["%s:%s:exon%d:c.%s%d%s:p.%s%d%s" % (i["gene"],
					i["acc"].split('/')[0], # torno all'accession originale
					i["exon_number"],
					i["cFROM"],
					i["coding_pos"],
					i["cTO"],
					i["AA_from"],
					i["AA_pos"],
					i["AA_to"]) for i in best_effect]
			variations_for_biologists = ','.join(var_info)
			# print ILLUMINATO!
			print "%s\t%s\t%s\t%s\t%f\t%s\t%s" % (
					"exonic",
					best_effect[0]["effect"],
					variations_for_biologists,
					#"%s:%s>%s" % tuple(change),
					"%s>%s" % (best_effect[0]["from_codon"],best_effect[0]["to_codon"]), # il cambio di codone
					get_codon_usage_ratio(best_effect[0]["from_codon"],best_effect[0]["to_codon"],chrom), # codon_usage_ratio
					real_ref_base,
					'\t'.join(row)
				)
		else:
			pos_type_list = []
			var_info = []
			for i in type_order:
				x = [j["pos_type"] for j in accessions_info if j["pos_type"]==i]
				y = ["%s:%s:::"%(j["gene"],j["acc"].split('/')[0]) for j in accessions_info if j["pos_type"]==i]
				if x:
					pos_type_list.extend(x)
					var_info.extend(y)
			if len(set(pos_type_list))==1: # sono tutti dello stesso tipo
				pos_type_list = pos_type_list[0]
			else: # li elenco tutti
				pos_type_list = ','.join(pos_type_list)
			effect = "NA"
			#if accessions_info:
			#var_info = ["%s:%s:::"%(i["gene"],i["acc"].split('/')[0]) for i in accessions_info]
			variations = ','.join(var_info)
			#else:
			#	variations= '.' # no gene

			#pos_type_list = ','.join(set([i["pos_type"] for i in accessions_info]))
			#effect = "NA"
			#if accessions_info:
			#	var_info = ["%s:%s:::"%(i["gene"],i["acc"].split('/')[0]) for i in accessions_info]
			#	variations = ','.join(var_info)
			#else:
			#	variations= '.' # no gene
			print "%s\t%s\t%s\t%s\t%s\t%s\t%s" % (
						pos_type_list,
						effect,
						variations,
						#"%s:%s>%s" % tuple(change),
						'.', # il cambio di codone
						'.', # codon_usage_ratio
						'?', # real_ref_base
						'\t'.join(row)
						)

	# final report
	if len(wrong_input_rows) > 0:
		print >>sys.stderr, "#------------------------------------#"
		print >>sys.stderr, "WARNING: Skipped %d wrong input rows." % len(wrong_input_rows)
		print >>sys.stderr, "First 5 wrong input rows:"
		for i in wrong_input_rows[:5]:
			print >>sys.stderr, '\t'.join(map(str,i))
		print >>sys.stderr, "#------------------------------------#"
		return False
	return True

def revcomp(b):
	conv = {
		'A': 'T',
		'C': 'G',
		'G': 'C',
		'T': 'A',
		'N': 'N',
		}
	return conv[b.upper()]

def translate(codon,info=""):
	genetic_code = {
		"GCT":'A',
		"GCC":'A',
		"GCA":'A',
		"GCG":'A',
		"CGT":'R',
		"CGC":'R',
		"CGA":'R',
		"CGG":'R',
		"AGA":'R',
		"AGG":'R',
		"AAT":'N',
		"AAC":'N',
		"GAT":'D',
		"GAC":'D',
		"TGT":'C',
		"TGC":'C',
		"CAA":'Q',
		"CAG":'Q',
		"GAA":'E',
		"GAG":'E',
		"GGT":'G',
		"GGC":'G',
		"GGA":'G',
		"GGG":'G',
		"CAT":'H',
		"CAC":'H',
		"ATT":'I',
		"ATC":'I',
		"ATA":'I',
		"ATG":'M',
		"TTA":'L',
		"TTG":'L',
		"CTT":'L',
		"CTC":'L',
		"CTA":'L',
		"CTG":'L',
		"AAA":'K',
		"AAG":'K',
		"TTT":'F',
		"TTC":'F',
		"CCT":'P',
		"CCC":'P',
		"CCA":'P',
		"CCG":'P',
		"TCT":'S',
		"TCC":'S',
		"TCA":'S',
		"TCG":'S',
		"AGT":'S',
		"AGC":'S',
		"ACT":'T',
		"ACC":'T',
		"ACA":'T',
		"ACG":'T',
		"TGG":'W',
		"TAT":'Y',
		"TAC":'Y',
		"GTT":'V',
		"GTC":'V',
		"GTA":'V',
		"GTG":'V',
		"TAA":'*',
		"TGA":'*',
		"TAG":'*'
	}
	if len(codon)!=3:
		sys.exit("ERROR: wrong codon length! (%s) Info: %s" % (codon,str(info)))
	return genetic_code[codon.upper()]

def get_codon_usage_ratio(codon1,codon2,chrom):
	# codon usage per il genoma
	CU = {
		"AAA":24.4,
		"AAC":19.1,
		"AAG":31.9,
		"AAT":17.0,
		"ACA":15.1,
		"ACC":18.9,
		"ACG":6.1,
		"ACT":13.1,
		"AGA":12.2,
		"AGC":19.5,
		"AGG":12.0,
		"AGT":12.1,
		"ATA":7.5,
		"ATC":20.8,
		"ATG":22.0,
		"ATT":16.0,
		"CAA":12.3,
		"CAC":15.1,
		"CAG":34.2,
		"CAT":10.9,
		"CCA":16.9,
		"CCC":19.8,
		"CCG":6.9,
		"CCT":17.5,
		"CGA":6.2,
		"CGC":10.4,
		"CGG":11.4,
		"CGT":4.5,
		"CTA":7.2,
		"CTC":19.6,
		"CTG":39.6,
		"CTT":13.2,
		"GAA":29.0,
		"GAC":25.1,
		"GAG":39.6,
		"GAT":21.8,
		"GCA":15.8,
		"GCC":27.7,
		"GCG":7.4,
		"GCT":18.4,
		"GGA":16.5,
		"GGC":22.2,
		"GGG":16.5,
		"GGT":10.8,
		"GTA":7.1,
		"GTC":14.5,
		"GTG":28.1,
		"GTT":11.0,
		"TAA":1.0,
		"TAC":15.3,
		"TAG":0.8,
		"TAT":12.2,
		"TCA":12.2,
		"TCC":17.7,
		"TCG":4.4,
		"TCT":15.2,
		"TGA":1.6,
		"TGC":12.6,
		"TGG":13.2,
		"TGT":10.6,
		"TTA":7.7,
		"TTC":20.3,
		"TTG":12.9,
		"TTT":17.6
	}

	# codon usage per il mitocondrio
	CUmito = {
		"AAA":23.6,
		"AAC":34.1,
		"AAG":3.0,
		"AAT":10.6,
		"ACA":32.7,
		"ACC":41.5,
		"ACG":2.6,
		"ACT":14.6,
		"AGA":0.4,
		"AGC":9.7,
		"AGG":0.4,
		"AGT":3.5,
		"ATA":44.1,
		"ATC":51.4,
		"ATG":12.3,
		"ATT":33.9,
		"CAA":20.5,
		"CAC":18.0,
		"CAG":2.7,
		"CAT":4.0,
		"CCA":11.8,
		"CCC":33.4,
		"CCG":1.8,
		"CCT":11.3,
		"CGA":6.5,
		"CGC":5.7,
		"CGG":0.8,
		"CGT":2.8,
		"CTA":70.0,
		"CTC":38.5,
		"CTG":13.1,
		"CTT":16.9,
		"GAA":16.5,
		"GAC":14.0,
		"GAG":7.8,
		"GAT":4.5,
		"GCA":23.8,
		"GCC":29.6,
		"GCG":3.2,
		"GCT":14.0,
		"GGA":19.5,
		"GGC":20.8,
		"GGG":9.6,
		"GGT":8.9,
		"GTA":19.5,
		"GTC":14.1,
		"GTG":6.6,
		"GTT":10.7,
		"TAA":1.6,
		"TAC":22.2,
		"TAG":1.1,
		"TAT":12.9,
		"TCA":19.2,
		"TCC":22.9,
		"TCG":2.4,
		"TCT":9.7,
		"TGA":21.6,
		"TGC":4.6,
		"TGG":2.4,
		"TGT":1.7,
		"TTA":17.3,
		"TTC":37.2,
		"TTG":6.0,
		"TTT":17.8,
	}

	if chrom!="chrM":
		return CU[codon2]/CU[codon1]
	else: # mitocodnrio
		return CUmito[codon2]/CUmito[codon1]

################# fine funzioni ##################################
##################################################################

# controllo l'input
if sys.argv[1] not in OPTIONS:
	sys.exit("ERROR: %s: wrong option: %s.\nAvailable options are:\n\t%s" % (sys.argv[0],sys.argv[1], "\t\n".join(OPTIONS)))

PAR0,PAR1,PAR2,PAR3 = sys.argv[:4]

if PAR1 == "--from-file":
	try:
		fd_yar = open(PAR2)
	except IOError:
		sys.exit("ERROR: %s: file '%s' does not exist! Exit." % (PAR0,PAR2))
	try:
		fd_input = (PAR3=='-') and sys.stdin or open(PAR3)
	except IOError:
		sys.exit("ERROR: %s: file '%s' does not exist! Exit." % (PAR0,PAR3))
	# annoy!!
	x = annoy(fd_input,fd_yar)
	if x is True:
		print >>sys.stderr, "Done."
	else:
		print >>sys.stderr, "Done_but_some_row_skipped."
elif PAR1 == "--retrieve-file":
	genome_symbol = PAR2
	outdir = PAR3
	# controlli
	if not (os.path.isdir(outdir) and os.access(outdir, os.W_OK)):
		sys.exit("ERROR: %s: output directory '%s' not writable. Exit." % (PAR0,outdir))
	outfile_yar = os.path.join(outdir, "refFlat_mRNA.%s.yar" % genome_symbol)
	outfile_genome = os.path.join(outdir, "%s.fa" % genome_symbol)

	# download genome file
	genome_URL = BASE_GENOME % (genome_symbol,genome_symbol)
	cmd1 = "curl -# -f -O '%s'" % genome_URL
	cmd2 = "tar -Oxzf *chromFa.tar.gz > %s" % (outfile_genome,)
	cmd3 = "rm -f *chromFa.tar.gz"
	ret1 = subprocess.call(cmd1,shell=True)
	#if ret1!=0:
	#	sys.exit("ERROR: %s: command failed with code %d:\n%s" % (PAR0,ret1,cmd1))
	ret2 = subprocess.call(cmd2,shell=True)
	if ret2!=0:
		sys.exit("ERROR: %s: command failed with code %d:\n%s" % (PAR0,ret2,cmd2))
	ret3 = subprocess.call(cmd3,shell=True)
	if ret3!=0:
		sys.exit("ERROR: %s: command failed with code %d:\n%s" % (PAR0,ret3,cmd3))

	# download refFlat file
	refFlat_URL = BASE_REFFLAT % genome_symbol
	cmd4 = "curl -# %s | gunzip" % refFlat_URL

	temp_refFlat_stream =  subprocess.Popen(cmd4, stdout=subprocess.PIPE, shell=True).stdout
	# create the .yar file
	retrieve_correct_mRNA(temp_refFlat_stream,outfile_genome,outfile_yar)

##	# cancella il refFlat temporaneo
	print >>sys.stderr, "Done."
