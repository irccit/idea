#!/usr/bin/env bash
# vim: set softtabstop=2 shiftwidth=2 filetype=sh expandtab:
# Usage: idea count
# Summary: Run samtools mpileup and filter_and_count.py
# Help: This command is used to run samtools mpileup through the
# filter_and_count.py script to get a count file for each position of the target
# region.

# TODO: add a mismatch filtering step.

set -e
set -u
set -o pipefail

source "$_IDEA_ROOT/lib/logging"

export LC_ALL=C

progname="$(basename $0)"

eval "$("$_IDEA_ROOT/libexec/parseargs" "$progname" \
  "a" "aln"            1 1 "NA"   "Alignment file." \
  "b" "bed"            0 1 "NA"   "BED file of regions where pileup should be generated." \
  "C" "idea-conf"      1 1 "NA"   "IDEA configuration." \
  "d" "outdir"         0 1 "$PWD" "Output directory." \
  "m" "memory"         0 1 "1G"   "GNU sort buffer size." \
  "n" "num-mismatches" 0 1 -1     "Number of mismatches." \
  "Q" "min-quality"    0 1 "20"   "Minimum base quality for a base to be considered." \
  "s" "sample"         1 1 "NA"   "Sample name." \
  "t" "threads"        0 1 1      "Number of threads." \
  "w" "work-dir"       0 1 "NA"   "Working directory." \
  "--" \
  "$@")"

source "$_IDEA_ROOT/lib/getconf" "$idea_conf"

if [ -p "$aln" ]; then
  fail "$progname: Index file is needed, reading from a pipe is not supported."
  exit 1
fi

if [ ! -f "$aln" ]; then
  fail "$progname: $aln does not exist"
  exit 1
fi

if [[ "$aln" = *.sam ]]; then
  fail "$progname: Index file is needed, choose either BAM o CRAM input"
  exit 1
elif [[ "$aln" = *.bam ]]; then
  index_ext="bai"
elif [[ "$aln" = *.cram ]]; then
  index_ext="crai"
else
  warn "$progname: Unknown file extension for $aln"
  index_ext=""
fi

if [ -n "$index_ext" ]; then
  aln_index="$aln.$index_ext"
  if [ ! -f "$aln_index" ]; then
    warn "$progname: Missing index file, trying to build it"
    samtools index "$aln"
  fi
fi

if [ "$bed" != "NA" ]; then
  if [ -f "$bed" ]; then
    samtools_opts="-l\ $bed"
  else
    fail "$progname: $bed does not exist"
    exit 1
  fi
else
  warn "$progname: No BED file provided"
  samtools_opts="\ "
fi

mkdir -p "$outdir"

orig_outdir="$outdir"

if [ "$work_dir" != "NA" ]; then
  outdir="$(mktemp -d "$work_dir/$(basename $0).XXXXXX")"
fi

function counts_parallel() {
  # $1: alignment file
  # $2: output directory
  # $3: mininum quality
  # $4: chromosome
  # $5: genome sequence
  # $6: sample name
  # $7: samtools options
  # $8: base directory

  local aln="$1"
  local outdir="$2"
  local min_quality="$3"
  local chrom="$4"
  local genome_seq="$5"
  local sample="$6"
  local samtools_opts="$7"
  local num_mismatches="$8"

  filter_mismatches.py -u \
    -n "$num_mismatches" \
    -r "$chrom" \
    "$aln" | \
  samtools mpileup $samtools_opts \
    -B \
    -d 1000000 \
    -f "$genome_seq" \
    -q 1 \
    -Q "$min_quality" \
    -x \
    - 2>"$outdir/err_SamTools_mpileup_${sample}_$chrom" | \
  filter_and_count_mpileup.py 0 \
    - 2>"$outdir/err_filter_${sample}_$chrom" | \
  awk 'BEGIN {OFS = "\t"} {print $1, $2, $3 + $7, $4 + $8, $5 + $9, $6 + $10}'
}

export -f counts_parallel

function merge_error_files() {
  # $1: output directory
  # $2: sample name

  local outdir="$1"
  local sample="$2"

  sort "$outdir/err_SamTools_mpileup_${sample}_"* | \
  uniq -c > "$outdir/err_SamTools_mpileup_$sample"

  rm -f "$outdir/err_SamTools_mpileup_${sample}_"*

  sort "$outdir/err_filter_${sample}_"* | \
  uniq -c > "$outdir/err_filter_$sample"

  rm -f "$outdir/err_filter_${sample}_"*
}

outfile_name="$sample.Q$min_quality.counts.gz"

function cleanup() {
  merge_error_files "$outdir" "$sample"

  if [ "$work_dir" != "NA" ]; then
    mv "$outdir"/* "$orig_outdir"
  fi

  if [ "$work_dir" != "NA" ]; then
    rm -rf "$outdir"
  fi
}

trap cleanup ERR EXIT

info "$progname: Starting samtools mpileup on $sample"

samtools view -H "$aln" | \
awk '/^@SQ/ {n = split($2, a, ":"); print a[2]}' | \
parallel -j "$threads" \
  --tmpdir "$outdir" \
  counts_parallel "$aln" \
    "$outdir" \
    "$min_quality" \
    {} \
    "$genome_seq" \
    "$sample" \
    "$samtools_opts" \
    "$num_mismatches" | \
sort --parallel="$threads" \
  -T "$outdir" \
  -S "$memory" | \
gzip > "$outdir/$outfile_name"

success "$progname: Finished samtools mpileup on $sample"

exit 0
