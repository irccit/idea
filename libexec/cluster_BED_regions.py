#!/usr/bin/python
import sys

N_REGIONS = 100
TOLLERANCE = 0. # era un tentativo di migliorare lo split...

if len(sys.argv) < 1+1:
	sys.exit("""
	Usage: %s BED_file|- [N_REGIONS=%d]

	Output is a BED file, where the 'name' is the '/'-joined original coordinates belonging to the region. 
	""" % (sys.argv[0], N_REGIONS))

if len(sys.argv) > 2:
	N_REGIONS = int(sys.argv[2])
File = (sys.argv[1]=='-') and sys.stdin or open(sys.argv[1])
BED = [i.strip().split() for i in File]
#full.pop(0) # tolgo l'header

print >>sys.stderr, "Calculating %d regions ..." % N_REGIONS

###################################################
def get_segments(seg, min_dist=0): # returns (start,end, name)
	sorted_intervals = sorted(seg)

	if not sorted_intervals:  # no intervals to merge
		return

	# low and high represent the bounds of the current run of merges
	low, high = sorted_intervals[0][:2]
	names = ["%s-%s" % (low,high)] # contiene tutti gli intervalli

	for iv in sorted_intervals[1:]:
		if iv[0] <= high + min_dist:  # new interval overlaps current run
			high = max(high, iv[1])  # merge with the current run
			names.append("%s-%s" % (iv[0],iv[1]))
		else:  # current run is over
			yield low, high, names  # yield accumulated interval
			low, high = iv[:2]  # start new run
			names = ["%s-%s" % (low,high)]

	yield low, high, names  # end the final run
####################################################

data = {}
tot_depth = 0.0

for i in BED:
	chrom = i[0]
	start = int(i[1])+1 # BED e' 0-based
	end = int(i[2])
#	name = i[3]
	score = int(i[4])
	tot_depth += score ##end-start+1
	if not data.has_key(chrom):
		data[chrom] = []
	data[chrom].append((start, end, score))

print >>sys.stderr, "Total depth:", tot_depth

# ordino per start coordinate
for k,v in data.items():
	data[k] = sorted(v)

mean_depth = tot_depth/N_REGIONS

print >>sys.stderr, "Mean depth:", mean_depth

for chrom, coords in data.items():
	region = []
	D = 0
	for x in coords:
		s, e, score = x
		if region == []: # aggiungo comunque
#			print "aggiungo comunque:", [s,e, score]
			region = [s,e, score]
		elif D + score <= mean_depth: # aggiungo
			# aggiungo l'end
#			print "aggiungo END:", [s,e, score], D+score
			region[1] = e
			region[2] += score
		elif D + score < mean_depth*(1+TOLLERANCE):
#			print "aggiungo ancora END:", [s,e,score], D + score
			region[1] = e
			region[2] += score
		elif region: # scrivo la vecchia regione
#			print "scarico vecchia regione:", region, "min:", mean_depth*(1-TOLLERANCE), "max:", mean_depth*(1+TOLLERANCE), "D+score", D+score
			print "%s\t%d\t%d\t%s" % (chrom, region[0], region[1], score)
			region = [s,e, score]
			D = 0 # azzero
		D += score

	if region:
		print "%s\t%d\t%d\t%s" % (chrom, region[0], region[1], score)
