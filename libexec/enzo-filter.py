#!/usr/bin/python
# -*- coding: utf-8 -*-
# vim: set softtabstop=4 shiftwidth=4 expandtab:

import argparse
import fileinput
import sys
import scipy.stats

BASES = ['A', 'C', 'G', 'T']

def alleleFreqs(alleles, depth1, depth2):
    freq1 = []
    freq2 = []
    for a, n in alleles:
        freq1.append((a[0] / float(depth1), n))
        freq2.append((a[1] / float(depth2), n))

    return freq1, freq2

def getDeltaFreqs(freq1, freq2):
    return [(abs(f2[0] - f1[0]), f1[1], f2[1]) for f1, f2 in zip(freq1, freq2)]

def getRatioFreqs(freq1, freq2):
    ratioFreqs = []
    for i in range(len(freq1)): # same as len(freq2), by construction
        f1 = freq1[i][0]
        f2 = freq2[i][0]
        n = freq1[i][1] # same as freq2[i][1], by construction
        if f1 == f2:
            ratioFreqs.append((0, n)) # FIXME: why not 1? Ask Giorgio.
        elif f1 * f2 == 0:
            ratioFreqs.append((1000000, n)) # FIXME: why not float("inf")? Ask Giorgio.
        else:
            ratioFreqs.append((f1 / f2, n))
            ratioFreqs.append((f2 / f1, n))

    return ratioFreqs

def getPValues(alleles, depth1, depth2):
    pValues = []
    for a in alleles:
        c1, c2 = a[0]
        n = a[1]
        if c1 + c2 == 0: # FIXME: does it ever happen?
            pValues.append((10, n))
            continue

        pValue = scipy.stats.binom_test(c2, depth2, c1 / float(depth1))
        pValues.append((pValue, n))

    return pValues

def main():
    desc = "Filter joined count file for mutation calling."

    parser = argparse.ArgumentParser(description = desc)

    parser.add_argument("-a", "--allele", type = int, required = True,
            help = "Minimum allele read count.")
    parser.add_argument("-d", "--depth", type = int, required = True,
            help = "Minimum position depth.")
    parser.add_argument("-f", "--fisher", action = "store_true",
            default = False, help = "Calculate p-values using Fisher's test")
    parser.add_argument("joined_counts_file", help = "FIXME")

    args = parser.parse_args()

    for row in fileinput.input(args.joined_counts_file):
        row = row.strip().split('\t')

        if len(row) < 10:
            sys.exit("%s: error: wrong column number" % sys.argv[0])

        counts1 = map(float, row[2:6])
        counts2 = map(float, row[6:10])

        depth1 = sum(counts1)
        depth2 = sum(counts2)
        if depth1 < args.depth or depth2 < args.depth:
            continue

        # alleles present
        z = zip(counts1, counts2)

        alleles = []
        for i in range(4):
            a = z[i]
            if sum(a) > args.allele:
                alleles.append((a, i))

        if len(alleles) < 2:
            continue

        freq1, freq2 = alleleFreqs(alleles, depth1, depth2)

        # frequencies delta
        deltaFreqs = getDeltaFreqs(freq1, freq2)
        maxDeltaFreqs = max(deltaFreqs)

        # frequencies ratio
        ratioFreqs = getRatioFreqs(freq1, freq2)
        maxRatioFreqs = max(ratioFreqs)

        # p-value, with raw counts
        pValues = []
        if args.fisher:
            pValues = getPValues(alleles, depth1, depth2)
        else:
            pValues = [(-1, 3)]
        minPValues = min(pValues)

        print "%f\t%s\t%f\t%s\t%f\t%s\t%s" % (maxDeltaFreqs[0],
                ''.join(sorted(set([BASES[a1] for d, a1, a2 in deltaFreqs]))),
                maxRatioFreqs[0],
                ''.join(sorted(set([BASES[a] for d, a in ratioFreqs]))),
                minPValues[0],
                BASES[minPValues[1]],
                '\t'.join(row))

if __name__ == "__main__":
    main()
