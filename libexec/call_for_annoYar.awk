# vim: set softtabstop=4 shiftwidth=4 filetype=awk expandtab:

BEGIN {
    FS = "\t"
    OFS = "\t"

    MIN_FREQ = allele_freq # 0.1

    b["A"] = 9
    b["C"] = 10
    b["G"] = 11
    b["T"] = 12
}

{
    # $2: allele da delta_freq
    # $4: allele da ratio_freq

    CN1 = $9 + $10 + $11 + $12
    CN2 = $13 + $14 + $15 + $16

    # alleli presenti?
    n = split($2, alleles, "")
    FROM = ""
    TO = ""
    for (i = 1; i <= n; i++) {
        allele = alleles[i]
        # sample 1
        freq = $b[allele] / CN1
        if (freq >= MIN_FREQ)
            FROM = FROM allele
        # sample 2
        freq = $(b[allele] + 4) / CN2
        if(freq >= MIN_FREQ)
            TO = TO allele
    }
    call = FROM ">" TO

    # ricavo la call per ANNOVAR a partire da FROM e TO
    n1 = split(FROM, ff, "")
    n2 = split(TO, tt, "")

    ANNO_FROM = ""
    ANNO_TO = ""
    # cosa c'è soltanto in FROM
    COMMON1 = ""
    for(i = 1; i <= n1; i++) {
        present = 0
        for(j = 1; j <= n2; j++) {
            if (ff[i] == tt[j]) {
                COMMON1 = COMMON1 ff[i]
                present = 1
                break
            }
        }
        if (present == 0)
            ANNO_FROM = ANNO_FROM ff[i]
    }
    # cosa c'è soltanto in TO
    COMMON2 = ""
    for(j = 1; j <= n2; j++) {
        present = 0
        for(i = 1; i <= n1; i++) {
            if (ff[i] == tt[j]) {
                COMMON2 = COMMON2 ff[i]
                present = 1
                break
            }
        }
        if (present == 0)
            ANNO_TO = ANNO_TO tt[j]
    }

    if (ANNO_FROM == ANNO_TO) { # no change!
        # print "NO_CHANGE", $0 > "/dev/stderr"
        next
    }

    # controllo: troppi alleli!!
    if (length(ANNO_FROM) > 1 || length(ANNO_TO) > 1) {
        print "problema: troppi alleli:", \
                 "ANNO_FROM:", \
                 "."ANNO_FROM".", \
                 "ANNO_TO:", \
                 "."ANNO_TO".", \
                 $0 > "/dev/stderr"
        next
    }

    # gain/loss allele:
    if (ANNO_TO != "")
        GAIN_LOSS_ALLELE = ANNO_TO
    else if (ANNO_FROM != "")
        GAIN_LOSS_ALLELE = ANNO_FROM
    else
        print "PROBLEMA..." > "/dev/stderr"

    split($7, pos, ":")

    # CN1 = $9 + $10 + $11 + $12
    # CN2 = $13 + $14 + $15 + $16

    freq_to1 = $(b[GAIN_LOSS_ALLELE]) / CN1
    freq_to2 = $(b[GAIN_LOSS_ALLELE] + 4) / CN2
    gain_loss = (freq_to2 >= freq_to1) ? "GAIN_"GAIN_LOSS_ALLELE : "LOSS_"GAIN_LOSS_ALLELE

    # "riempio" uno dei due con il COMMON se vuoto...
    if (ANNO_FROM == "" && length(COMMON1) == 1) {
        ANNO_FROM = COMMON1
    }

    if (ANNO_TO == "" && length(COMMON2) == 1) {
        ANNO_TO = COMMON2
    }

    if (ANNO_FROM == "" || ANNO_TO == "") {
        print "problema: non so come scegliere l allele", \
                 "ANNO_FROM:", \
                 "."ANNO_FROM".", \
                 "ANNO_TO:", \
                 "."ANNO_TO".", \
                 $0 > "/dev/stderr"
        next
    }

#     per ANNOVAR, metto al primo posto il RIF <<<<<<<<<<<<<<<<<< non piu" necessario con ANNOyAR
#     SWAP = "unSwapped"
#     if (ANNO_TO == $8) { # devo scambiarli
#         ANNO_TO = ANNO_FROM
#         ANNO_FROM = $8
#         SWAP = "SWAPPED"
#         call = "(" call ")"
#     }
#
#     if(ANNO_FROM != $8){
#         print "prob per ANNOyAR...", \
#               ANNO_FROM,ANNO_TO, \
#               $0 > "/dev/stderr"
#         next
#     }

    print pos[1], \
             pos[2], \
             pos[2], \
             ANNO_FROM, \
             ANNO_TO, \
             "---", \
             $0, \
             call, \
             gain_loss # NON scrivo se ho swappato o no....
}
