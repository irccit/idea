#!/usr/bin/env bats
# vim: set softtabstop=2 shiftwidth=2 filetype=sh expandtab:

function setup() {
  export _OUTDIR="$(mktemp -p "$BATS_TMPDIR" -d "idea-map-test.XXXXXX")"

  if [ -s "$IDEA_CONF" ]; then
    source "$IDEA_CONF"
  else
    echo "E: you need to store IDEA configuration file path in \$IDEA_CONF"
    return 1
  fi
}

function teardown() {
  rm -rf "$_OUTDIR"
}

@test "idea-map with standard output" {
  # cannot use bats' run command if you need to redirect stdout
  idea map --stdout \
    -1 test_R1.fastq.gz \
    -2 test_R2.fastq.gz \
    -C "$IDEA_CONF" \
    -d  "$_OUTDIR" \
    -s "test-stdout" \
    -t $(nproc) > "$_OUTDIR/idea-map-stdout.sam"

  [ -s "$_OUTDIR/idea-map-stdout.sam" ]
}

@test "idea-map with single-end sample" {
  run idea map -1 test_R1.fastq.gz \
    -C "$IDEA_CONF" \
    -d "$_OUTDIR" \
    -s "test_single_end" \
    -t $(nproc) \
    -w "$BATS_TMPDIR"

  [ "$status" -eq 0 ]
  [ -s "$_OUTDIR/test_single_end.BWA.${genome_version}.sorted.cram" ]
}

@test "idea-map with paired-end sample" {
  run idea map -1 test_R1.fastq.gz \
    -2 test_R2.fastq.gz \
    -C "$IDEA_CONF" \
    -d "$_OUTDIR" \
    -s "test_paired_end" \
    -t $(nproc) \
    -w "$BATS_TMPDIR"

  [ "$status" -eq 0 ]
  [ -s "$_OUTDIR/test_paired_end.BWA.${genome_version}.sorted.cram" ]
}

@test "idea-map with trim on single-end sample" {
  run idea map -1 test_R1.fastq.gz \
    -C "$IDEA_CONF" \
    -d "$_OUTDIR" \
    -s "test_single_end" \
    -t $(nproc) \
    --trim 3 \
    -w "$BATS_TMPDIR"

  [ "$status" -eq 0 ]
  [ -s "$_OUTDIR/test_single_end.BWA.${genome_version}.sorted.cram" ]
}

@test "idea-map with trim on paired-end sample" {
  run idea map -1 test_R1.fastq.gz \
    -2 test_R2.fastq.gz \
    -C "$IDEA_CONF" \
    -d "$_OUTDIR" \
    -s "test_paired_end" \
    -t $(nproc) \
    --trim 3 \
    -w "$BATS_TMPDIR"

  [ "$status" -eq 0 ]
  [ -s "$_OUTDIR/test_paired_end.BWA.${genome_version}.sorted.cram" ]
}

@test "idea-map with process substituion for input" {
  run idea map -1 <(zcat test_R1.fastq.gz) \
    -2 <(zcat test_R2.fastq.gz) \
    -C "$IDEA_CONF" \
    -d "$_OUTDIR" \
    -s "test_process_substitution" \
    -t $(nproc) \
    -w "$BATS_TMPDIR"

  [ "$status" -eq 0 ]
  [ -s "$_OUTDIR/test_process_substitution.BWA.${genome_version}.sorted.cram" ]
}

@test "idea-map with BAM output" {
  run idea map -B \
    -1 test_R1.fastq.gz \
    -2 test_R2.fastq.gz \
    -C "$IDEA_CONF" \
    -d "$_OUTDIR" \
    -s "test_bam_output" \
    -t $(nproc) \
    -w "$BATS_TMPDIR"

  [ "$status" -eq 0 ]
  [ -s "$_OUTDIR/test_bam_output.BWA.${genome_version}.sorted.bam" ]
}

@test "idea-map with non-existent read1" {
  run idea map -B \
    -1 non_existing.fastq.gz \
    -2 test_R2.fastq.gz \
    -C "$IDEA_CONF" \
    -d "$_OUTDIR" \
    -s "test_bam_output" \
    -t $(nproc) \
    -w "$BATS_TMPDIR"

  [ "$status" -eq 1 ]
}

@test "idea-map with non-existent read2" {
  run idea map -B \
    -1 non_existing.fastq.gz \
    -2 test_R2.fastq.gz \
    -C "$IDEA_CONF" \
    -d "$_OUTDIR" \
    -s "test_bam_output" \
    -t $(nproc) \
    -w "$BATS_TMPDIR"

  [ "$status" -eq 1 ]
}
