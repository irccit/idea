#!/usr/bin/env bats
# vim: set softtabstop=2 shiftwidth=2 filetype=sh expandtab:

function setup() {
  export _OUTDIR="$(mktemp -p "$BATS_TMPDIR" -d "idea-count-mismatches-test.XXXXXX")"

  if [ -s "$IDEA_CONF" ]; then
    source "$IDEA_CONF"
  else
    echo "E: you need to store IDEA configuration file path in \$IDEA_CONF"
    return 1
  fi
}

function teardown() {
  rm -rf "$_OUTDIR"
}

@test "idea-count-mismatches CRAM output" {
  cp -a "test.BWA.${genome_version}.sorted.cram" \
    "test.BWA.${genome_version}.sorted.cram.crai" \
    "$_OUTDIR"

  run idea count-mismatches -a "$_OUTDIR/test.BWA.${genome_version}.sorted.cram" \
    -d "$_OUTDIR" \
    -s "test" \
    -w "$BATS_TMPDIR"

  [ "$status" -eq 0 ]
  [ ! -e "$_OUTDIR/test.BWA.${genome_version}.sorted.cram" ]
  [ ! -e "$_OUTDIR/test.BWA.${genome_version}.sorted.cram.crai" ]
  [ -s "$_OUTDIR/test.BWA.${genome_version}.sorted.mm-count.cram" ]
  [ -s "$_OUTDIR/test.BWA.${genome_version}.sorted.mm-count.cram.crai" ]
}

@test "idea-count-mismatches BAM output" {
  cp -a "test.BWA.${genome_version}.sorted.cram" \
    "test.BWA.${genome_version}.sorted.cram.crai" \
    "$_OUTDIR"

  run idea count-mismatches -B \
    -a "$_OUTDIR/test.BWA.${genome_version}.sorted.cram" \
    -d "$_OUTDIR" \
    -s "test" \
    -w "$BATS_TMPDIR"

  [ "$status" -eq 0 ]
  [ ! -e "$_OUTDIR/test.BWA.${genome_version}.sorted.cram" ]
  [ ! -e "$_OUTDIR/test.BWA.${genome_version}.sorted.cram.crai" ]
  [ -s "$_OUTDIR/test.BWA.${genome_version}.sorted.mm-count.bam" ]
  [ -s "$_OUTDIR/test.BWA.${genome_version}.sorted.mm-count.bam.bai" ]
}

@test "idea-count-mismatches keep input alignment" {
  cp -a "test.BWA.${genome_version}.sorted.cram" \
    "test.BWA.${genome_version}.sorted.cram.crai" \
    "$_OUTDIR"

  run idea count-mismatches -K \
    -a "$_OUTDIR/test.BWA.${genome_version}.sorted.cram" \
    -d "$_OUTDIR" \
    -s "test" \
    -w "$BATS_TMPDIR"

  [ "$status" -eq 0 ]
  [ -s "$_OUTDIR/test.BWA.${genome_version}.sorted.cram" ]
  [ -s "$_OUTDIR/test.BWA.${genome_version}.sorted.cram.crai" ]
  [ -s "$_OUTDIR/test.BWA.${genome_version}.sorted.mm-count.cram" ]
  [ -s "$_OUTDIR/test.BWA.${genome_version}.sorted.mm-count.cram.crai" ]
}
