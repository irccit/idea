#!/usr/bin/env bats
# vim: set softtabstop=2 shiftwidth=2 filetype=sh expandtab:

function setup() {
  export _OUTDIR="$(mktemp -p "$BATS_TMPDIR" -d "idea-map-test.XXXXXX")"

  if [ -s "$IDEA_CONF" ]; then
    source "$IDEA_CONF"
  else
    echo "E: you need to store IDEA configuration file path in \$IDEA_CONF"
    return 1
  fi
}

function teardown() {
  rm -rf "$_OUTDIR"
}

@test "idea-trim with standard output" {
  # cannot use bats' run command if you need to redirect stdout
  idea trim -i "test_R1.fastq.gz" > "$_OUTDIR/test_R1.trim.fastq"

  [ -s "$_OUTDIR/test_R1.trim.fastq" ]
}

@test "idea-trim with output directory" {
  run idea trim -i "test_R1.fastq.gz" \
    -d "$_OUTDIR" \
    -n 3 \
    -t $(nproc) \
    -w "$BATS_TMPDIR"

  [ "$status" -eq 0 ]
  [ -s "$_OUTDIR/test_R1.trim.fastq.gz" ]
}

@test "idea-trim with non-existent input" {
  run idea trim -i "non-existing.fastq.gz" \
    -d "$_OUTDIR" \
    -n 3 \
    -t $(nproc) \
    -w "$BATS_TMPDIR"

  [ "$status" -eq 1 ]
}
