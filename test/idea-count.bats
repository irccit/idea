#!/usr/bin/env bats
# vim: set softtabstop=2 shiftwidth=2 filetype=sh expandtab:

function setup() {
  export _OUTDIR="$(mktemp -p "$BATS_TMPDIR" -d "idea-count-test.XXXXXX")"

  if [ -s "$IDEA_CONF" ]; then
    source "$IDEA_CONF"
  else
    echo "E: you need to store IDEA configuration file path in \$IDEA_CONF"
    return 1
  fi
}

function teardown() {
  echo "$_OUTDIR"
  # rm -rf "$_OUTDIR"
}

@test "idea-count standard" {
  run idea count -a "test.BWA.hg38.sorted.mm-count.cram" \
    -C "$IDEA_CONF" \
    -d "$_OUTDIR" \
    -s "test" \
    -t $(nproc) \
    -w "$BATS_TMPDIR"

  [ "$status" -eq 0 ]
  [ -s "$_OUTDIR/test.Q20.counts.gz" ]
}

@test "idea-count with BED" {
  run idea count -a "test.BWA.hg38.sorted.mm-count.cram" \
    -b "hg38_refFlat.IRCC-target.codingExons.bed" \
    -C "$IDEA_CONF" \
    -d "$_OUTDIR" \
    -s "test" \
    -t $(nproc) \
    -w "$BATS_TMPDIR"

  [ "$status" -eq 0 ]
  [ -s "$_OUTDIR/test.Q20.counts.gz" ]
}

@test "idea-count with mismatches filter" {
  run idea count -a "test.BWA.hg38.sorted.mm-count.cram" \
    -C "$IDEA_CONF" \
    -d "$_OUTDIR" \
    -n 3 \
    -s "test" \
    -t $(nproc) \
    -w "$BATS_TMPDIR"

  [ "$status" -eq 0 ]
  [ -s "$_OUTDIR/test.Q20.counts.gz" ]
}


@test "idea-count with quality filter" {
  run idea count -a "test.BWA.hg38.sorted.mm-count.cram" \
    -C "$IDEA_CONF" \
    -d "$_OUTDIR" \
    -Q 30 \
    -s "test" \
    -t $(nproc) \
    -w "$BATS_TMPDIR"

  [ "$status" -eq 0 ]
  [ -s "$_OUTDIR/test.Q30.counts.gz" ]
}

@test "idea-count with non-existent alignment" {
  run idea count -a "non-existing.cram" \
    -C "$IDEA_CONF" \
    -d "$_OUTDIR" \
    -s "test" \
    -t $(nproc) \
    -w "$BATS_TMPDIR"

  [ "$status" -eq 1 ]
}

@test "idea-count with non-existent BED" {
  run idea count -a "test.BWA.hg38.sorted.mm-count.cram" \
    -b "non-existing.bed" \
    -C "$IDEA_CONF" \
    -d "$_OUTDIR" \
    -s "test" \
    -t $(nproc) \
    -w "$BATS_TMPDIR"

  [ "$status" -eq 1 ]
}

