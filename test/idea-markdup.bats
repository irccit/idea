#!/usr/bin/env bats
# vim: set softtabstop=2 shiftwidth=2 filetype=sh expandtab:

function setup() {
  export _OUTDIR="$(mktemp -p "$BATS_TMPDIR" -d "idea-markdup-test.XXXXXX")"

  if [ -s "$IDEA_CONF" ]; then
    source "$IDEA_CONF"
  else
    echo "E: you need to store IDEA configuration file path in \$IDEA_CONF"
    return 1
  fi
}

function teardown() {
  rm -rf "$_OUTDIR"
}

@test "idea-markdup CRAM output" {
  cp -a "test.BWA.${genome_version}.sorted.cram" \
    "test.BWA.${genome_version}.sorted.cram.crai" \
    "$_OUTDIR"

  run idea markdup -C "$IDEA_CONF" \
    -a "$_OUTDIR/test.BWA.${genome_version}.sorted.cram" \
    -d "$_OUTDIR" \
    -s "test" \
    -t $(nproc) \
    -w "$BATS_TMPDIR"

  [ "$status" -eq 0 ]
  [ ! -e "$_OUTDIR/test.BWA.${genome_version}.sorted.cram" ]
  [ ! -e "$_OUTDIR/test.BWA.${genome_version}.sorted.cram.crai" ]
  [ -s "$_OUTDIR/test.BWA.${genome_version}.sorted.MarkDuplicates.cram" ]
  [ -s "$_OUTDIR/test.BWA.${genome_version}.sorted.MarkDuplicates.cram.crai" ]
}

@test "idea-markdup BAM output" {
  cp -a "test.BWA.${genome_version}.sorted.cram" \
    "test.BWA.${genome_version}.sorted.cram.crai" \
    "$_OUTDIR"

  run idea markdup -B \
    -C "$IDEA_CONF" \
    -a "$_OUTDIR/test.BWA.${genome_version}.sorted.cram" \
    -d "$_OUTDIR" \
    -s "test" \
    -t $(nproc) \
    -w "$BATS_TMPDIR"

  [ "$status" -eq 0 ]
  [ ! -e "$_OUTDIR/test.BWA.${genome_version}.sorted.cram" ]
  [ ! -e "$_OUTDIR/test.BWA.${genome_version}.sorted.cram.crai" ]
  [ -s "$_OUTDIR/test.BWA.${genome_version}.sorted.MarkDuplicates.bam" ]
  [ -s "$_OUTDIR/test.BWA.${genome_version}.sorted.MarkDuplicates.bam.bai" ]
}

@test "idea-markdup keep input alignment" {
  cp -a "test.BWA.${genome_version}.sorted.cram" \
    "test.BWA.${genome_version}.sorted.cram.crai" \
    "$_OUTDIR"

  run idea markdup -K \
    -C "$IDEA_CONF" \
    -a "$_OUTDIR/test.BWA.${genome_version}.sorted.cram" \
    -d "$_OUTDIR" \
    -s "test" \
    -t $(nproc) \
    -w "$BATS_TMPDIR"

  [ "$status" -eq 0 ]
  [ -s "$_OUTDIR/test.BWA.${genome_version}.sorted.cram" ]
  [ -s "$_OUTDIR/test.BWA.${genome_version}.sorted.cram.crai" ]
  [ -s "$_OUTDIR/test.BWA.${genome_version}.sorted.MarkDuplicates.cram" ]
  [ -s "$_OUTDIR/test.BWA.${genome_version}.sorted.MarkDuplicates.cram.crai" ]
}

@test "idea-markdup with non-existent input alignment" {
  run idea markdup -C "$IDEA_CONF" \
    -a non_existiong_alignment.cram \
    -d "$_OUTDIR" \
    -s "test" \
    -t $(nproc) \
    -w "$BATS_TMPDIR"

  [ "$status" -eq 1 ]
}

@test "idea-markdup with input from /dev/stdin" {
  cp -a "test.BWA.${genome_version}.sorted.cram" \
    "test.BWA.${genome_version}.sorted.cram.crai" \
    "$_OUTDIR"

  run bash -c 'samtools view "$_OUTDIR/test.BWA.${genome_version}.sorted.cram" | \
    idea markdup -C "$IDEA_CONF" \
      -a /dev/stdin \
      -d "$_OUTDIR" \
      -s "test" \
      -t $(nproc) \
      -w "$BATS_TMPDIR"'

  echo $status

  [ "$status" -eq 1 ]
}

@test "idea-markdup with input from command substitution" {
  cp -a "test.BWA.${genome_version}.sorted.cram" \
    "test.BWA.${genome_version}.sorted.cram.crai" \
    "$_OUTDIR"

  run idea markdup -C "$IDEA_CONF" \
    -a <(samtools view "$_OUTDIR/test.BWA.${genome_version}.sorted.cram") \
    -d "$_OUTDIR" \
    -s "test" \
    -t $(nproc) \
    -w "$BATS_TMPDIR"

  [ "$status" -eq 1 ]
}
