#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set softtabstop=4 shiftwidth=4 expandtab:

import argparse
import pysam

def main():
    desc = '''Filter each alignment based on the number of mismatches'''

    parser = argparse.ArgumentParser(description = desc)

    output_format = parser.add_mutually_exclusive_group()

    output_format.add_argument("-b", "--bam", action = "store_true",
            help = "Output BAM")

    output_format.add_argument("-u", "--uncompressed", action = "store_true",
            help = "Output uncrompressed BAM")

    parser.add_argument("-n", "--num-mismatches", default = -1, type = int,
            help = "Number of mismatches")

    # https://github.com/pysam-developers/pysam/issues/399
    # FIXME: once fix for pysam's issue #399 gets released, default becomes "-"
    parser.add_argument("-r", "--region", default = None, help = "Region in "
            "which mismatches are filtered")

    parser.add_argument("-o", "--output", default = "-", help = "Output file")

    parser.add_argument("aln", nargs = 1, help = "Alignment file")

    args = parser.parse_args()

    if args.bam:
        mode = "wb"
    elif args.uncompressed:
        mode = "wbu"
    else:
        mode = "wc"

    with pysam.AlignmentFile(args.aln[0]) as input_aln:
        with pysam.AlignmentFile(args.output, mode, header = input_aln.header) as output_aln:
            for input_aln_seg in input_aln.fetch(region = args.region,
                    multiple_iterators = False):
                if input_aln_seg.has_tag("YM"):
                    if (args.num_mismatches < 0 or
                            input_aln_seg.get_tag("YM") <= args.num_mismatches):
                        output_aln.write(input_aln_seg)
                else:
                    output_aln.write(input_aln_seg)

if __name__ == '__main__':
    main()
