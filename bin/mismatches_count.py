#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set softtabstop=4 shiftwidth=4 expandtab:

import argparse
import pysam
import re

def count_mismatches(aln_seg):
    if not aln_seg.has_tag('MD'):
        assert(aln_seg.is_unmapped)
        return aln_seg

    count = 0
    md_pat = re.compile(r'(\d+)|([ACGTN])|(\^[ACGTN+])')
    for match, mismatch, deletion in md_pat.findall(aln_seg.get_tag('MD')):
        if mismatch:
            count += 1

    aln_seg.set_tag('YM', count)

    return aln_seg

def main():
    desc = '''Add a tag to each alignment containing the number of mismatches.'''

    parser = argparse.ArgumentParser(description = desc)

    parser.add_argument("-B", "--bam", action = "store_true", help = "Output BAM")

    parser.add_argument("-o", "--output", default = "-", help = "Output file")

    parser.add_argument("aln", nargs = 1, help = "Alignment file")

    args = parser.parse_args()

    if args.bam:
        mode = "wb"
    else:
        mode = "wc"

    with pysam.AlignmentFile(args.aln[0]) as input_aln:
        with pysam.AlignmentFile(args.output, mode, header = input_aln.header) as output_aln:
            for input_aln_seg in input_aln.fetch(until_eof = True):
                output_aln_seg = input_aln_seg
                output_aln_seg = count_mismatches(output_aln_seg)
                output_aln.write(output_aln_seg)

if __name__ == "__main__":
    main()
