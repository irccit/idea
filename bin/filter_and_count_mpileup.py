#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set softtabstop=4 shiftwidth=4 expandtab:

import argparse
import fileinput
import re
import sys

NUMBERS = ['0','1','2','3','4','5','6','7','8','9']
COLUMNS = ['A','C','G','T','a','c','g','t']
PATT = re.compile("\^.")

def purge_pileup(ref_base, bases):
    if '^' in bases:
        bases = re.sub(PATT, "", bases)
    if '$' in bases:
        bases = bases.replace("$", "")
    if '.' in bases:
        bases = bases.replace(".", ref_base)
    if ',' in bases:
        bases = bases.replace(",", ref_base.lower())

    if '+' not in bases and '-' not in bases:
        return bases

    bases_purged = []

    pos = 0
    length = len(bases)
    while length - pos > 0:
        base = bases[pos]
        if base in ["+", "-"]:
            num = []
            for i in xrange(pos + 1, length):
                if bases[i] in NUMBERS:
                    num.append(bases[i])
                else:
                    shift = int("".join(num))
                    break
            pos = i + shift
        else:
            bases_purged.append(base)
            pos += 1

    return bases_purged

def filter_and_count(min_quality, row):
    row_split = row.split("\t")

    if len(row_split) == 6:
        chrom, pos, ref_base, depth, bases, qualities = row_split
    elif len(row_split) >= 4 and row_split[3] == "0":
        print "%s:%s\t%s\t0\t0\t0\t0\t0\t0\t0\t0" % (row_split[0],
                row_split[1], row_split[2].upper())
        return
    else:
        print >> sys.stderr, "WARNING: wrong line format (%s)" % row
        return

    ref_base = ref_base.upper()
    bases_purged = purge_pileup(ref_base, bases)

    if len(bases_purged) != len(qualities):
        sys.exit("%s: error: after purge bases and qualities string must have"
                "the same length" % sys.argv[0])

    bases_purged = [base for base, qual in zip(bases_purged, qualities) if
            ord(qual) >= min_quality]

    counts = ["%s:%s" % (chrom, pos), ref_base]
    # This ignores indel (*) in pileup string.
    counts.extend([bases_purged.count(col) for col in COLUMNS])

    print "\t".join(map(str, counts))

def main():
    desc = "Parse samtools mpileup output to generate an 8-columns count file."

    parser = argparse.ArgumentParser(description = desc)

    parser.add_argument('min_quality', type = int, help = "minimum base "
            "quality for a base to be considered (Phred+33).")

    parser.add_argument('mpileup_file', help = "samtools mpileup input file "
            "or '-' for stdin.")

    args = parser.parse_args()

    if args.min_quality > 40:
        sys.exit("%s: error: min_quality must be Phred+33. (%d)"
                % (sys.argv[0], args.min_quality))

    print >> sys.stderr, "WARNING: filter_and_count_mpileup: 8-fields count file!"

    args.min_quality = args.min_quality + 33

    for row in fileinput.input(args.mpileup_file):
        filter_and_count(args.min_quality, row.strip())

if __name__ == "__main__":
    main()
