#!/usr/bin/env bash
# vim: set softtabstop=2 shiftwidth=2 filetype=sh expandtab:

set -e
set -u
set -o pipefail
set +o posix

export LC_ALL=C

PROGRAM_NAME="$(basename $0)"

# Given a counts file and a BED file, compute average/median depth for the BED
# target.

if [ $# -ne 3 ]; then
  echo "Usage: $PROGRAM_NAME <positions.bed> <counts_file[.gz]|-> <mean|median>" 1>&2
  echo "       counts_file must be sorted for join!" 1>&2
  echo "       counts_file could be 1x4 or 1x8" 1>&2
  exit 1
fi

POSITIONS="$1"
INPUT="$2"
OPT="$3"

if [ "$OPT" != "mean" ] && [ "$OPT" != "median" ]; then
  echo "$PROGRAM_NAME: wrong option. Specifiy either 'mean' or 'median'." 1>&2
  exit 1
fi

if [ -p "$INPUT" ]; then
  echo "$PROGRAM_NAME: Reading from a pipe is not supported." 1>&2
  exit 1
fi

# Cleanup trap
function control_c() {
  rm -f "$POS" "$DATA_TMP"
}
trap control_c ERR EXIT

# Creo elenco di posizioni
POS="$(mktemp --tmpdir="$PWD" "__tmp_$PROGRAM_NAME\__.XXXXXXXXXXXX")"

mawk 'BEGIN { FS = "\t" }
  {
    for (i = $2 + 1; i <= $3; i++) {
      print $1":"i
    }
  }' $POSITIONS | \
sort -u -T . -S 4G > "$POS"

if [ "$OPT" = "mean" ]; then
  join -t ' ' $POS <(zcat -f $INPUT) | \
    mawk 'BEGIN {
      FS = "\t"
      OFS = "\t"
    }

    {
      x = ($3 + $4 + $5 + $6 + $7 + $8 + $9 + $10)
      data += x
      if (x > 0)
        N++
    }

    END {
      print data / N
    }'
elif [ "$OPT" = "median" ]; then
  DATA_TMP="$(mktemp --tmpdir="$PWD" "__tmp_$PROGRAM_NAME\__.XXXXXXXXXXXX")"

  join -t '	' "$POS" <(zcat -f "$INPUT") | \
  mawk 'BEGIN { FS = "\t" }
    {
      x = $3 + $4 + $5 + $6 + $7 + $8 + $9 + $10
      if (x > 0)
        print x
    }' | \
  sort -g -T . -S 4G > "$DATA_TMP"

  N="$(cat "$DATA_TMP" | wc -l)"

  mawk -v N="$N" 'BEGIN { pos = (N % 2 == 1) ? (N + 1) / 2 : (N / 2 + 1) }
    {
      if (NR == pos) {
        if(N % 2 == 1)
          diploidy = $1
        else
          diploidy = ($1 + prev) / 2

        print diploidy
        exit
      }
      prev = $1
    }' "$DATA_TMP"
fi

exit 0
