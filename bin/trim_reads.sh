#!/usr/bin/env bash
# vim: set softtabstop=2 shiftwidth=2 filetype=sh expandtab:

set -e
set -u
set -o pipefail



if [ $# -ne 2 ]; then
  echo "Usage: $(basename $0) N file.fq[.gz]" >&2
  echo >&2
  echo "Trim the first and the last N bases." >&2
  exit 1
fi

bp="$1"
input="$2"

zcat -f "$input" | \
mawk -v BP="$bp" '{
if(NR % 2 == 0)
  print substr($1, (BP + 1), ((length($1) - BP) - BP))
else
  print
}'
